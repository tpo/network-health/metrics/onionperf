lxml
matplotlib
networkx
nose-cov
nose
numpy
pandas < 1.4.0
scipy
seaborn >= 0.11
stem >= 1.7.0
tgentools @ git+https://github.com/shadow/tgen.git@main#egg=tgentools&subdirectory=tools
requests
sphinx
